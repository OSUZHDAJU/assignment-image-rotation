#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

struct __attribute__ ((__packed__)) pixel { uint8_t b, g, r; };
struct image {
    size_t width, height;
    struct pixel* data;
};
struct image* image_create( size_t width, size_t height );
void image_free(struct image* img);

#endif
