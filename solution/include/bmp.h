#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <inttypes.h>
#include <stdio.h>

enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_OUT_OF_MEMORY
};

enum  write_status  {
    WRITE_OK,
    WRITE_ERROR
};
enum read_status from_bmp( FILE const* in, struct image* img);
enum write_status to_bmp( FILE const* out, struct image const* img );
#endif
