#include "../include/file.h"
#include "../include/error.h"
#include <stdio.h>
#include <stdlib.h>

extern FILE* file_read(const char* pathname) {
    FILE* in = fopen(pathname, "rb");
    if (in == NULL) {
        print_err("No such file:\n");
        print_err(pathname);
        print_err("\n");
    }
    return in;
}

extern FILE* file_write(const char* pathname) {
    return fopen(pathname, "wb");
}

extern enum file_status close(FILE* file) {
    if (fclose(file) == 0) {
        return OK;
    }
    return ERROR;
}
