#include "../include/bmp.h"
#include <alloca.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_TYPE 0x4D42
#define DWORD_SIZE 4
#define BMP_VALID_BIT_COUNT 24
#define BMP_RESERVED 0
#define BMP_HEADER_SIZE 40
#define BMP_PIXEL_PER_METER 2834
#define BMP_PLANES 1
#define BMP_CIR_USED 0
#define BMP_CIR_IMPORTANT 0
#define BMP_COMPRESSION 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t get_padding(const size_t width) {
    return DWORD_SIZE - (width * sizeof(struct pixel)) % DWORD_SIZE;
}

static void create_header(struct bmp_header* header, const size_t width, const size_t height) {
    const uint8_t padding = get_padding(width);
    const size_t image_size = (sizeof(struct pixel) * width + padding) * height;
    header->bfType = BMP_TYPE;
    header->biBitCount = BMP_VALID_BIT_COUNT;
    header->biXPelsPerMeter = BMP_PIXEL_PER_METER;
    header->biYPelsPerMeter = BMP_PIXEL_PER_METER;
    header->bfileSize = image_size + sizeof(struct bmp_header);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP_PLANES;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_CIR_USED;
    header->biClrImportant = BMP_CIR_IMPORTANT;
}

extern enum read_status from_bmp( FILE const* in, struct image* img) {
    const struct bmp_header header;

    if (fread((struct bmp_header*) &header, sizeof(struct bmp_header), 1,(FILE *) in) != 1 || header.bfType != BMP_TYPE)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != BMP_VALID_BIT_COUNT)
        return READ_INVALID_HEADER;


    const size_t width = img -> width = header.biWidth;
    const size_t height = img -> height = header.biHeight;

    struct pixel* pixels = malloc(width * height * sizeof(struct pixel));

    if (pixels == NULL) return READ_OUT_OF_MEMORY;

    if (fseek((FILE *) in, header.bOffBits, SEEK_SET) != 0) {
        free(pixels);
        return READ_INVALID_HEADER;
    }

    uint8_t padding = get_padding(img->width);
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + i * width, width * sizeof(struct pixel), 1, (FILE *) in) != 1){
            free(pixels);
            return READ_INVALID_BITS;
        }
        if (fseek((FILE *) in, padding, SEEK_CUR) != 0) {
            free(pixels);
            return READ_INVALID_BITS;
        }
    }

    img->data = pixels;
    return READ_OK;
}
extern enum write_status to_bmp( FILE const* out, struct image const* img ) {
    if (out == NULL || img == NULL || img->data == NULL) return WRITE_ERROR;

    const struct bmp_header* header = alloca(sizeof(struct bmp_header));
    if (header == NULL) return WRITE_ERROR;
    create_header((struct bmp_header*) header, img->width, img->height);

    if (fwrite(header, sizeof(struct bmp_header), 1, (FILE *)out) != 1) {
        return WRITE_ERROR;
    }

    if (fseek((FILE *)out, header->bOffBits, SEEK_SET) != 0) {
        return WRITE_ERROR;
    }

    const size_t width = img->width;
    const size_t height = img->height;
    const uint8_t padding = get_padding(width);
    uint8_t* null_bits = calloc(1, padding);
    if (null_bits == NULL) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < height; i++) {
        if (fwrite(img->data + width * i, width * sizeof(struct pixel),1, (FILE *)out) != 1 ) {
            free(null_bits);
            return WRITE_ERROR;
        }
        if (fwrite(null_bits, padding, 1, (FILE *) out) != 1) {
            free(null_bits);
            return WRITE_ERROR;
        }
    }

    free(null_bits);

    return WRITE_OK;
}
